import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Material } from './material.import';
import { APP_ROUTING } from './app.routes';
import { HomePage } from './pages/home/home.page';
import { AppService } from './services/app.service';
import { CardComponent } from './components/card/card.component';
import { NgPipesModule, FilterByPipe } from 'ngx-pipes';
import { FormsModule } from '@angular/forms';
import { ComparativePage } from './pages/comparative/comparative.page';
import { MultipleFilterByPipe } from './multiple-filter-by.pipe';
import { PictureComponent } from './components/picture/picture.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    CardComponent,
    ComparativePage,
    MultipleFilterByPipe,
    PictureComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    Material,
    APP_ROUTING
  ],
  providers: [
    AppService,
    NgPipesModule,
    FilterByPipe,
    MultipleFilterByPipe,
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: false}}
  ],
  entryComponents: [
    PictureComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
