import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as axios from 'axios';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
    private readonly apiUrl = 'http://localhost:58651/api-cars.asmx/';
    private readonly headers = { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } };
    constructor(private http: HttpClient) { }
    private readonly cars = [
        {
            "carPk": 1,
            "carBrand": "Toyota",
            "carModel": "Rav4",
            "carYear": "1995",
            "carPrice": 6000,
            "carCreatedDate": "28/8/2018 16:19:59",
            "pictures": [
                {
                    "picturePk": 1,
                    "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/maxresdefault.jpg?alt=media\u0026token=cf85c804-348b-4b07-a46f-61d15c955745"
                }]
        },
        {
            "carPk": 2,
            "carBrand": "Daihatsu",
            "carModel": "Terios",
            "carYear": "2012",
            "carPrice": 15000,
            "carCreatedDate": "29/8/2018 19:50:18",
            "pictures": [
                {
                    "picturePk": 2, "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/terios.jpg?alt=media\u0026token=b0be5120-4bf1-4c50-98d3-30a4f2d450e6"
                }]
        },
        {
            "carPk": 5,
            "carBrand": "Ferrari",
            "carModel": "F50",
            "carYear": "2013",
            "carPrice": 1000000,
            "carCreatedDate": "29/8/2018 19:50:18",
            "pictures": [
                {
                    "picturePk": 5, "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/ferrari.jpg?alt=media\u0026token=a9513a0c-ea72-4a4d-9023-9bf11a406eec"
                }]
        },
        {
            "carPk": 6,
            "carBrand": "Chevrolet",
            "carModel": "Camaro",
            "carYear": "2005",
            "carPrice": 50000,
            "carCreatedDate":
                "29/8/2018 19:50:18",
            "pictures": [
                {
                    "picturePk": 6,
                    "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/camaro.jpg?alt=media\u0026token=df2437d4-82f8-44a5-a327-8cd9f7f7a347"
                }]
        },
        {
            "carPk": 7,
            "carBrand": "Jeep",
            "carModel": "Wrangler",
            "carYear": "2008",
            "carPrice": 12000,
            "carCreatedDate": "29/8/2018 19:50:18",
            "pictures": [
                {
                    "picturePk": 7,
                    "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/cars-2802.appspot.com/o/wrangler.jpg?alt=media\u0026token=ee408643-0f7e-4739-9561-8a8fe7e129db"
                }]
        }]

    // public listCars = () => {
    //     let body = `user=my_user&password=my_password`
    //     return axios.default.post(`${this.apiUrl}ListAllCars`, body, this.headers);
    // }

    public getJSON(): Observable<any> {
        return this.http.get("./assets/data.json")
    }
}