import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'picture-component',
    templateUrl: './picture.component.html',
    styleUrls: ['./picture.component.css']
})
export class PictureComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public imageUrl: string) { }

    ngOnInit() { }
}