import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { PictureComponent } from '../picture/picture.component';
import * as moment from 'moment';

@Component({
    selector: 'card-component',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

    constructor(private matDialog: MatDialog) {

    }

    @Input() carElement: any;
    @Output() comparativeSelectedEvent = new EventEmitter<any>();
    public createdDate: any;

    ngOnInit() {
        this.createdDate = moment(this.formatDate(this.carElement.carCreatedDate)).locale('es').fromNow();
    }
    
    formatDate = (inputDate) => {
        let date = inputDate.split(' ')[0];
        let time = inputDate.split(' ')[1];
        return new Date(date.split('/')[2], date.split('/')[1] - 1, date.split('/')[0], time.split(':')[0], time.split(':')[1], time.split(':')[2]);
    }

    comparativeSelected = ($event, pk) => {
        this.comparativeSelectedEvent.emit({ id: pk, value: $event.checked })
    }

    openImage = (imageUrl) => {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = false;
        dialogConfig.autoFocus = true;
        dialogConfig.maxHeight = '400px';
        dialogConfig.maxWidth = '700px';
        dialogConfig.data = {
            imageUrl: imageUrl
        };
        this.matDialog.open(PictureComponent, dialogConfig);
    }
}