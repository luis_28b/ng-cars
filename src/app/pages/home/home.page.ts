import { Component, OnInit } from '@angular/core';
import { AppService } from '../../services/app.service';
import { FilterByPipe } from 'ngx-pipes';
import { MultipleFilterByPipe } from '../../multiple-filter-by.pipe';
import { Router } from '@angular/router';

@Component({
    selector: 'home-page',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.css']
})
export class HomePage implements OnInit {

    constructor(private _router: Router, private service: AppService, private filterBy: FilterByPipe, private multipleFilterBy: MultipleFilterByPipe) { }

    public carsList: any;
    public filteredCarsList: any;
    public filter = 'carBrand';
    public showFilter = false;
    public textFinder = '';
    public comparativeArray = [];

    ngOnInit() {
        this.service.getJSON().subscribe(res => {
            this.carsList = res;
            this.filteredCarsList = res;
        })
        // this.service.listCars().then((result) => {
        //     if(result.data.success) {
        //         this.carsList = result.data.PLURAL_MODEL_NAME;
        //         this.filteredCarsList = this.carsList;
        //     }
        //     else {

        //     }         
        // }).catch((error) => {

        // })
    }

    displayFilter = () => {
        this.filter = 'carBrand';
        this.showFilter = !this.showFilter;
        if (!this.showFilter) {
            this.filteredCarsList = this.carsList;
        }
    }

    onCriteriaChanged = (criteria) => {
        this.filter = criteria;
        this.textFinder = '';
    }

    onFilterChanged = () => {
        this.filteredCarsList = this.filterBy.transform(this.carsList, [this.filter], this.textFinder);
    }

    onComparativeSelected = (car) => {
        if (!car.value) this.comparativeArray.splice(this.comparativeArray.indexOf(car.id), 1);
        else this.comparativeArray.push(car.id);
    }

    compare = () => {
        if (this.comparativeArray.length === 3) {
            let selectedCars = this.multipleFilterBy.transform(this.carsList, 'carPk', this.comparativeArray);
            localStorage.setItem('cars', JSON.stringify(selectedCars));
            this._router.navigate(['/comparative']);
        }
    }
}