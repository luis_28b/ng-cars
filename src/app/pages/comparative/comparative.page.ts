import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'comparative-page',
    templateUrl: './comparative.page.html',
    styleUrls: ['./comparative.page.css']
})
export class ComparativePage implements OnInit {

    public cars: any;

    ngOnInit() {
        this.cars = JSON.parse(localStorage.getItem('cars'));
    }
}