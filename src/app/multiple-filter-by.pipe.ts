import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multipleFilterBy'
})
export class MultipleFilterByPipe implements PipeTransform {

  transform(objectArray: any[], property: string, parameters: string[]): any[] {
    let result: any[] = [];
    objectArray.forEach(element => {
      parameters.forEach(element2 => {
        if (element[property] === element2) {
          result.push(element);
        }
      });
    });
    return result;
  }
}
