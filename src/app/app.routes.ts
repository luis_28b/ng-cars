import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './pages/home/home.page';
import { ComparativePage } from './pages/comparative/comparative.page';

const APP_ROUTES: Routes = [
    { path: 'home', component: HomePage },
    { path: 'comparative', component: ComparativePage },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });